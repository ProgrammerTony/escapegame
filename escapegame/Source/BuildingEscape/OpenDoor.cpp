// Fill out your copyright notice in the Description page of Project Settings.

#include "OpenDoor.h"
#include "GameFramework/Actor.h"

// Sets default values for this component's properties
UOpenDoor::UOpenDoor()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UOpenDoor::BeginPlay()
{
	Super::BeginPlay();
	//ActorThatOpens = GetWorld()->GetFirstPlayerController()->GetPawn();
	Owner = GetOwner();
	// ...
	
}

void UOpenDoor::OpenDoor()
{

	if (Rotator > -90.0f) 
	{
		Rotator--;
	}

	FRotator NewRotation = FRotator(0.0f, Rotator, 0.0f);
	Owner->SetActorRotation(NewRotation);
	//UE_LOG(LogTemp, Warning, TEXT("Rotating door"));
}

void UOpenDoor::CloseDoor()
{
	if (Rotator < 1.0f)
	{
		Rotator+=3.0f;
	}
	FRotator NewRotation = FRotator(0.0f, Rotator, 0.0f);
	Owner->SetActorRotation(NewRotation);
	//UE_LOG(LogTemp, Warning, TEXT("Rotating door"));
}


// Called every frame
void UOpenDoor::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	//if the actor that opens is in the volume
	//then open the door
	if (PressurePlate != nullptr) {
		if (GetTotalMassOfActorsOnPlate() > 40.0f)
		{
			OpenDoor();
			LastDoorOpenTime = GetWorld()->GetTimeSeconds();
		}

		if (GetWorld()->GetTimeSeconds() - LastDoorOpenTime > DoorCloseDelay)
		{
			CloseDoor();
		}
	}

	// ...
}

float UOpenDoor::GetTotalMassOfActorsOnPlate()
{
	float TotalMass = 0.0f;

	//Find all the overlapping actors
	TArray<AActor*> OverlappingActors;
	PressurePlate->GetOverlappingActors(OUT OverlappingActors);

	for (auto& Actor : OverlappingActors) 
	{
		TotalMass += Actor->FindComponentByClass<UPrimitiveComponent>()->GetMass();
		UE_LOG(LogTemp, Warning, TEXT("Name: %s on: Pressure Plate"), *Actor->GetName());
	}
	//Iterate through them adding their masses


	return TotalMass;
}

